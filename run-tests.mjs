import assert from "assert";
import fs from "fs/promises";
import path from "path";
import url from "url";
import vm from "vm";

let __filename = url.fileURLToPath(import.meta.url);
let __dirname = path.dirname(__filename);

async function mainAsync() {
  let testsDirectory = path.join(__dirname, "test");
  let files = await searchFilesRecursively(testsDirectory, []);

  for (let fileName of files) {
    await runtimeEntry(fileName);
  }
}

async function searchFilesRecursively(directory, fileNames) {
  let names = await fs.readdir(directory);
  fileNames = fileNames || [];

  for (let name of names) {
    let namePath = path.join(directory, "/", name);
    let nameStat = await fs.stat(namePath);

    if (nameStat.isDirectory()) {
      fileNames = await searchFilesRecursively(namePath, fileNames);
    } else {
      fileNames.push(namePath);
    }
  }

  return fileNames;
}

async function runtimeEntry(specifier) {
  let module = await runtimeLoad(specifier);
  if (module.status === "unlinked") await module.link(runtimeLoad);
  if (module.status === "linked") await module.evaluate();

  return module;
}

async function runtimeLoad(specifier) {
  let file = await fs.readFile(specifier);
  let context = vm.createContext({
    assert: assert,
    test: unitTestInjectFunction
  });

  return new vm.SourceTextModule(file.toString(), {
    identifier: specifier,
    importModuleDynamically: runtimeEntry,
    // context: context,
  });
}

async function unitTestInjectFunction(name, call) {
  try {
    await call();
    console.log(`Passed: ${name}`);
  } catch (err) {
    console.error(`\nTEST FAILED: ${name}`);
    console.error(err.stack);
  }
}

mainAsync().catch((err) => {
  console.error(err);
  process.exit(1);
});
