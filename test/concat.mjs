import { concat } from "../src/concat.mjs"

test("ensures that concat works", () => {
  let input = "test";

  let actual = concat(input);
  let expected = [2, "test"];

  assert.deepStrictEqual(actual, expected);
});